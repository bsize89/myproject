import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class AboutHattrickPageTest {

    private WebDriver driver;
    private HomePage homePage;
    private String url = "https://www.hattrick.org/";

    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver", "f:\\QA\\geckodriver\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        homePage = new HomePage(driver);
    }

    @Test
    public void getCom(){
        AboutHattrickPage aboutHattrickPage = homePage.clickAboutHattrickPage();
        String heading  = aboutHattrickPage.getHeadingText();
        Assert.assertEquals("О Hattrick", heading);
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
