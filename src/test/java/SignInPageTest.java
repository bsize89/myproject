

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SignInPageTest {
    private WebDriver driver;
    private HomePage homePage;
    private String url = "https://www.hattrick.org/";

    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver", "f:\\QA\\geckodriver\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        homePage = new HomePage(driver);
    }

    @Test
    public void signInAccount()  {


    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
