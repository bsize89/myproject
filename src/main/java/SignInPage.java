import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage {

    WebDriver driver;
   public SignInPage(WebDriver driver) {
        this.driver = driver;
    }

    private By personalAccountBtn = By.id("ctl00_CPContent_ucMessages_btnNext");
    private By heading = By.xpath("//div[@id='content']//h1");

    public SignInPage clickPersonalAccountBtn() {
        driver.findElement(personalAccountBtn).click();
        return new SignInPage(driver);
    }

    public String getHeadingText(){
        return driver.findElement(heading).getText();
    }

    public personalAccountPage loginPersonalAccount(){
        this.clickPersonalAccountBtn();
        return new personalAccountPage(driver);
    }
}
