import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


public class MainClass {
    static WebDriver driver;

    public static void main(String[] args) {
        String url = "https://www.hattrick.org/";

        System.setProperty("webdriver.gecko.driver", "f:\\QA\\geckodriver\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        //homePage.register("maks_oteam","123456", "dsdDs@fsfss.ru","rrrrrr");

    }
}
