import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AboutHattrickPage {
    WebDriver driver;

    public AboutHattrickPage(WebDriver driver) {
        this.driver = driver;
    }

    private By heading = By.xpath("//div[@class='boxBody']//h1");

    public String getHeadingText(){
        return driver.findElement(heading).getText();
    }


}
