import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.Select;


public class RegistrationPage {
    WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    private By heading = By.xpath("//div[@id='startpage_signup_box']//h2");
    private By loginName = By.id("signup-loginname");
    private By signupPassword = By.id("signup-password");
    private By signupConfirmPassword = By.id("signup-confirmPassword");
    private By signupEmail = By.id("signup-email");
    private By selectAge = By.id("signup-age-regular");
    private By selectLeague = By.id("signup-league");
    private By selectRegion = By.id("signup-region");
    private By signupTeamName = By.id("signup-teamname");
    private By acceptedAgreement = By.id("acceptedAgreement");
    private By signupButton = By.xpath("//input[(contains(@class, 'signupButton')) and not(contains(@disabled, 'disabled'))]");

    public RegistrationPage typeUserName(String username) {
        driver.findElement(loginName).sendKeys(username);
        return new RegistrationPage(driver);
    }

    public RegistrationPage typePassword(String password) {
        driver.findElement(signupPassword).sendKeys(password);
        return new RegistrationPage(driver);
    }

    public RegistrationPage typeConfirmPassword(String password) {
        driver.findElement(signupConfirmPassword).sendKeys(password);
        return new RegistrationPage(driver);
    }

    public RegistrationPage typeEmail(String email) {
        driver.findElement(signupEmail).sendKeys(email);
        return new RegistrationPage(driver);
    }


    public RegistrationPage typeSelectAge() {
        Select dropdown = new Select(driver.findElement(selectAge));
        dropdown.selectByVisibleText("16 и более");
        return new RegistrationPage(driver);
    }

    public RegistrationPage typeSelectLeague() {
        Select dropdown = new Select(driver.findElement(selectLeague));
        dropdown.selectByVisibleText("Belarus");
        return new RegistrationPage(driver);
    }

    public RegistrationPage typeSelectRegion() {
        Select dropdown = new Select(driver.findElement(selectRegion));
        dropdown.selectByVisibleText("Minsk");
        return new RegistrationPage(driver);
    }

    public RegistrationPage typeTeamName(String teamName) {
        driver.findElement(signupTeamName).sendKeys(teamName);
        return new RegistrationPage(driver);
    }

    public RegistrationPage clickAccept() {
        driver.findElement(acceptedAgreement).click();
        return new RegistrationPage(driver);
    }

    public RegistrationPage clickSignUpButton() {
        driver.findElement(signupButton).click();
            return new RegistrationPage(driver);
    }

    public RegistrationPage register(String username, String password, String email, String teamName ){

        this.typeUserName(username);
        this.typePassword(password);
        this.typeConfirmPassword(password);
        this.typeEmail(email);
        this.typeSelectAge();
        this.typeSelectLeague();
        this.typeSelectRegion();
        this.typeTeamName(teamName);
        this.clickAccept();
        this.clickSignUpButton();
        return new RegistrationPage(driver);
    }

    public String getHeadingText(){
        return driver.findElement(heading).getText();
    }

}
