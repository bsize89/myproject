import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
    private WebDriver driver;

    public HomePage(WebDriver driver) {

        this.driver = driver;
    }

    private By getCommandButton = By.cssSelector("a[href*=\"/Default.aspx\"]");
    private By aboutHattrickButton = By.xpath("//a[contains(text(),'О Hattrick')]");
    private By userNameField = By.id("ctl00_CPContent_ucLogin_txtUserName");
    private By passwordField = By.id("ctl00_CPContent_ucLogin_txtPassword");
    private By SignInBtn = By.id("ctl00_CPContent_ucLogin_butLogin");
    private By heading = By.xpath("//div[@id='content']//h1");

    public RegistrationPage clickGetCommandButton() {
        driver.findElement(getCommandButton).click();
        return new RegistrationPage(driver);
    }

    public  AboutHattrickPage clickAboutHattrickPage() {
        driver.findElement(aboutHattrickButton).click();
        return new AboutHattrickPage(driver);
    }

    public HomePage typeUserNameField(String username) {
        driver.findElement(userNameField).sendKeys(username);
        return new HomePage(driver);
    }

    public HomePage typePasswordField(String password) {
        driver.findElement(passwordField).sendKeys(password);
        return new HomePage(driver);
    }

    public SignInPage clickSignInBtn() {
        driver.findElement(SignInBtn).click();
        return new SignInPage(driver);
    }

    public SignInPage signInAccount(String username, String password){
        this.typeUserNameField(username);
        this.typePasswordField(password);
        this.clickSignInBtn();
        return new SignInPage(driver);
    }

    public String getHeadingText(){
        return driver.findElement(heading).getText();
    }
}
